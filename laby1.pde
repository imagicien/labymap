int COLORS_PER_PALETTE = 5;

public class Cellule
{
  public int x;
  public int y;
  
  public Cellule(int aX, int aY)
  {
    x = aX;
    y = aY;
  }
}

public class Labyrinthe
{
  public int N;
  public int M;
  public int ord;
  public boolean[][] lignes;
  public boolean[][] colonnes;
  public boolean[][] visites;
  public int[][] dist;
  public int[][] back;
  public Cellule fin;
  public Cellule debut; // trouvée pendant l'algo
  
  public Labyrinthe(int n, int m)
  {
    N = n;
    M = m;
    ord = max(N, M);
    lignes = new boolean[M+1][M];
    colonnes = new boolean[N+1][N];
    visites = new boolean[N][M]; // Tous à FAUX par défaut
    dist = new int[N][M];
    back = new int[N][M];
    
    // Initialiser tous les murs à VRAI
    for(int i = 0; i < ord+1; i++) // Sur-indice
    {
      for(int j = 0; j < ord; j++) // Sous-indice
      {
        if(i < M+1 && j < M) lignes[i][j] = true;
        if(i < N+1 && j < N) colonnes[i][j] = true;
      }
    }
    
    // GÉNÉRER LE LABYRINTHE ---------------------
    
    // 1. Choisir case fin au hasard
    fin = new Cellule(int(random(N)), int(random(M)));
    debut = fin;
    
    // 2. Commencer algo recursif
    Cellule c = fin;
    while(true)
    {
      // Marquer cellule comme visitée
      visites[c.x][c.y] = true;
      
      // Est-ce la case début?
      if(dist[c.x][c.y] >= dist[debut.x][debut.y])
        debut = c;
        
      // Prochain
      Cellule neighbor = getAnyUnvisitedNeighbor(c);
      if(neighbor != null)
      {
        // Avancer vers voisin
        removeWallBetween(c, neighbor);
        dist[neighbor.x][neighbor.y] = dist[c.x][c.y] + 1;
        back[neighbor.x][neighbor.y] = computeBacktrackCode(c, neighbor);
        c = neighbor;
      }
      else
      {
        // Revenir sur nos pas
        if(back[c.x][c.y] != 0)
        {
          c = backtrack(c);
        }
        else
        {
          break; // Retour à la case fin
        }
      }
    }
  }
  public int computeBacktrackCode(Cellule c, Cellule neighbor)
  {
    /*
    
      1
    4 X 2
      3
      
    */
    if(c.x == neighbor.x)
    {
      return neighbor.y > c.y ? 1 : 3;
    }
    else if(c.y == neighbor.y)
    {
      return neighbor.x > c.x ? 4 : 2;
    }
    else
    {
      throw new RuntimeException("Cas invalide");
    }
  }
  public Cellule backtrack(Cellule c)
  {
    int code = back[c.x][c.y];
    if(code == 1)      return new Cellule(c.x, c.y-1);
    else if(code == 2) return new Cellule(c.x+1, c.y);
    else if(code == 3) return new Cellule(c.x, c.y+1);
    else if(code == 4) return new Cellule(c.x-1, c.y);
    else throw new RuntimeException("Pas de backtrack");
  }
  public void removeWallBetween(Cellule c1, Cellule c2)
  {
    if(c1.x != c2.x && c1.y == c2.y)
    {
      // Diff en x, enlever colonne
      colonnes[max(c1.x, c2.x)][c1.y] = false;
    }
    else if(c1.y != c2.y && c1.x == c2.x)
    {
      // Diff en y, enlever ligne
      lignes[max(c1.y, c2.y)][c1.x] = false;
    }
    else
    {
      throw new RuntimeException("Cas invalide");
    }
  }
  public Cellule getAnyUnvisitedNeighbor(Cellule c)
  {
    ArrayList<Cellule> uvn = new ArrayList<Cellule>();
    
    if(c.x > 0 && !visites[c.x-1][c.y]) uvn.add(new Cellule(c.x-1, c.y));
    if(c.x < N-1 && !visites[c.x+1][c.y]) uvn.add(new Cellule(c.x+1, c.y));
    
    if(c.y > 0 && !visites[c.x][c.y-1]) uvn.add(new Cellule(c.x, c.y-1));
    if(c.y < M-1 && !visites[c.x][c.y+1]) uvn.add(new Cellule(c.x, c.y+1));
    
    if(uvn.size() == 0)
      return null;
    else if(uvn.size() == 1)
      return uvn.get(0);
    else
      return uvn.get(int(random(uvn.size())));
  }
  public void dessiner(boolean cases, boolean murs, color[] palette)
  {
    int largCase = width/N;
    int hautCase = height/M;
    float distMax = (float)(dist[debut.x][debut.y]);
    
    noStroke();
    
    // Case fin
    if(!cases)
    {
      fill(0.67, 1, 1);
      rect(fin.x * largCase, fin.y * hautCase, largCase, hautCase);
    }
    
    // Background
    if(cases)
    {
      for(int i = 0; i < N; i++)
      {
        for(int j = 0; j < M; j++)
        {
          //fill(dist[i][j] / distMax * 0.67, 1.0, 1.0);
          fill(paletteLerp(palette, dist[i][j] / distMax));
          rect(i * largCase, j * hautCase, largCase, hautCase);
          //text(dist[i][j], (i+0.5) * largCase, (j+0.5) * hautCase);
        }
      }
    }
    
    // Murs
    if(murs)
    {
      noFill();
      stroke(0, 0, 0);
      for(int i = 0; i < ord+1; i++) // Sur-indice
      {
        for(int j = 0; j < ord; j++) // Sous-indice
        {
          // Dessiner ligne i,j
          if(i < M+1 && j < M && lignes[i][j])
          {
            int x1 = j * largCase;
            int y1 = i * hautCase;
            int x2 = (j+1) * largCase;
            int y2 = i * hautCase;
            line(x1, y1, x2, y2);
          }
          if(i < N+1 && j < N && colonnes[i][j])
          {
            int x1 = i * largCase;
            int y1 = j * hautCase;
            int x2 = i * largCase;
            int y2 = (j+1) * hautCase;
            line(x1, y1, x2, y2);
          }
        }
      }
    }
    
  } // methode dessiner
  public color paletteLerp(color[] palette, float position)
  {
    float palettePosition = position * (COLORS_PER_PALETTE-1);
    int firstColorIndex = floor(palettePosition);
    color color1 = palette[firstColorIndex];
    color color2 = palette[ceil(palettePosition)];
    return lerpColor(color1, color2, palettePosition - firstColorIndex);
  }
} // classe Labyrinthe

color[][] couleurs = new color[][]{
  { #CE43A4, #FF0B4A, #A4D821, #C6F05E, #45C0BA }, // Electric Apple Tree
  { #E1FF00, #E60F5E, #E60F0F, #03E9C3, #FCFDF3 }, // Trippy1
  { #69D38D, #FF0000, #000000, #000000, #000000 }, // Scary1
  { #330521, #322A33, #4F3F40, #874F49, #FA1648 }, // scary & scarier
  { #EF3807, #B8AE5E, #5F651D, #2C3238, #3C1010 }, // contents flammable
  { #706869, #FF1C4D, #0E7D0F, #FFE300, #C4C4C4 }, // Blah
  { #630F2A, #3A0D1F, #1C1325, #023A47, #0F644D }  // TheDeepShadowFormula
};

void setup()
{
  size(800, 800);
  background(255);
  strokeWeight(2);
  colorMode(HSB, 1.0);
  
  Labyrinthe laby = new Labyrinthe(800, 800);
  laby.dessiner(true, false, couleurs[4]);
  save("laby.png");
}
